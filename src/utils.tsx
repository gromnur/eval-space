import {Picture} from "./Objects";

/**
 * Transforme en json en objetPicture
 * @param json
 */
export function transformJsonToPictures(json: any) : Picture {

    let picture:Picture = {
        copyright: json.data.copyright,
        date: json.data.date,
        explanation: json.data.explanation,
        hdurl: json.data.hdurl,
        title: json.data.title,
        url: json.data.url
    }
    return picture;
};

/**
 * Token pour utiliser l'API
 */
export const token = "Jkcv9NZ1sppPLxJperDn0OzE2rpdxpGmpDmQHDWG";
