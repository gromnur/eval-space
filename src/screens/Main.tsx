import React, {useEffect, useState} from 'react';
import {
    StyleSheet,
    SafeAreaView,
    ScrollView,
    Text,
    Image,
    View,
    ImageBackground,
    FlatList,
    TouchableOpacity,
} from 'react-native';
import Axios from "axios";
import {token, transformJsonToPictures} from "../utils";
import {Picture} from "../Objects";
import { Dimensions } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import ImageZoom from 'react-native-image-pan-zoom';

type Props = {}

const Main: React.FC<Props> = (props) => {

    const [currentPicture, setCurrentPicture] = useState<Picture>()
    const [pictures, setPictures] = useState<Picture[]>([])
    const [showContent, setShowContent] = useState<boolean>(true)

    useEffect(() => {
        getPictures()
    }, [])

    // On précharge les image HD car elle sont longue à charger
    useEffect(() => {
        if (currentPicture?.hdurl)
            preloadImage(currentPicture.hdurl)
    }, [currentPicture])

    const onPressMiniature = (item: Picture) => {
        setCurrentPicture(item)
    }

    /**
     * Charge l'image hd (plus longue à charger), améliore l'expérience utilisateurs
     * @param hdurl
     */
    const preloadImage = async (hdurl: string) => {
        await Image.prefetch(hdurl)
    }

    const showImageChange = () => {
        setShowContent(!showContent)
    }

    /**
     * Recupère l'image du jour
     */
    async function getPictures() {
        let date: Date = new Date;

        for (let i = 0 ; i < 10; i++) {
            // Création du string pour la date
            let day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate()
            let month = date.getMonth() < 10 ? "0" + date.getMonth()+1 : date.getMonth()+1
            let stringDate = date.getFullYear() + "-" + month + "-" +day

            // Appel à l'api
            let json = await Axios.get("https://api.nasa.gov/planetary/apod?api_key="+token+"&date="+stringDate)

            // Transformation en objet
            let picture = transformJsonToPictures(json)
            pictures.push(picture)
            setPictures([...pictures])

            // Set up du premier élément
            if (i == 0) {
                setCurrentPicture(pictures[0])
            }

            // Mise a jour de la date
            date.setDate(date.getDate() - 1 )
        }
    }

    const pagePrincipal = () => {
        return <ScrollView>
            <FlatList
                data={pictures}
                showsHorizontalScrollIndicator={false}
                horizontal={true}
                style={styles.list}
                keyExtractor={item => item.title}
                renderItem={({item, index}) =>
                { return <TouchableOpacity onPress={()=> onPressMiniature(item)}>
                    <Image source={{ uri: item?.url }} style={styles.miniature}/>
                </TouchableOpacity>}
                }
            />
            <TouchableOpacity onPress={showImageChange}>
                <ImageBackground source={{ uri: currentPicture?.url }} style={styles.image}>
                    <LinearGradient style={styles.gradient} colors={['rgba(0,0,0,1)', 'transparent']}/>
                    <View>
                        <LinearGradient style={styles.gradient} colors={['transparent', 'rgba(0,0,0,1)']}/>
                        <Text style={styles.title}>{currentPicture?.title} </Text>
                    </View>
                </ImageBackground>
            </TouchableOpacity>
            <Text style={styles.textDescription}>{currentPicture?.explanation}</Text>
        </ScrollView>
    }

    const appercuDetail = () => {
        return <View>
            <TouchableOpacity onPress={showImageChange}>
                <Text style={styles.goBack}><ArrowBackIcon/></Text>
            </TouchableOpacity>
            <ImageZoom style={styles.ImageZom}
                       cropWidth={Dimensions.get('window').width}
                       cropHeight={Dimensions.get('window').height*3/4}
                       imageWidth={Dimensions.get('window').width}
                       imageHeight={Dimensions.get('window').height*3/4}>
                <ImageBackground source={{ uri: currentPicture?.hdurl }} style={styles.image}>
                    <LinearGradient style={styles.gradient} colors={['rgba(0,0,0,1)', 'transparent']}/>
                    <LinearGradient style={styles.gradient} colors={['transparent', 'rgba(0,0,0,1)']}/>
                </ImageBackground>
            </ImageZoom>
            <View style={styles.fondNoir}/>
        </View>
    }

    /**
     * Affichage de l'écran
     */
    return (
        <SafeAreaView>
            {showContent ? (pagePrincipal()) : (appercuDetail())}
        </SafeAreaView>
    );
}
const styles = StyleSheet.create({
    goBack: {
        padding: 20,
        backgroundColor: 'black',
        color: 'white',
        height: Dimensions.get('window').height/8
    },
    ImageZom: {
        backgroundColor: 'black',
    },
    fondNoir: {
      backgroundColor: 'black',
      height:Dimensions.get('window').height/8
    },
    miniature: {
        margin: 10,
        borderRadius: 10,
        height: 80,
        width: 80,
        aspectRatio: 1,
    },
    image: {
        resizeMode: "contain",
        justifyContent: "space-between",
        display:"flex",
        aspectRatio: 0.5,
        width: Dimensions.get('window').width,
        height: Dimensions.get('window').height*3/4,
        backgroundColor: 'black'
    },
    gradient: {
        height: 50
    },
    textDescription: {
        paddingHorizontal: 50,
        color: "white",
        fontWeight: "bold",
        textAlign: "center",
        position: "relative",
        backgroundColor: "#000000",
    },
    title: {
        paddingHorizontal: 50,
        color: "white",
        fontSize: 38,
        fontWeight: "bold",
        textAlign: "center",
        backgroundColor: "#000000"
    },
    list: {
        backgroundColor: "black",
        height: 100,
        color: "red",
        fontWeight: "bold"
    },
});

export default Main
